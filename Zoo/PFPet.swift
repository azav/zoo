//
//  PFPet.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/27/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit
import Parse

class PFPet: PFObject, PFSubclassing {
	
	@NSManaged var name: String?
	@NSManaged var type: String?
	@NSManaged var location: String?
	@NSManaged var category: String?
	@NSManaged var price: String?
	@NSManaged var sex: String?
	@NSManaged var descript: String?
	@NSManaged var phone: String?
	@NSManaged var owner: PFUser?
	@NSManaged var pictureFile: PFFileObject?
	@NSManaged var deviceId: String?
	@NSManaged var userId: String?
	@NSManaged var userName: String?
	
	
	static func parseClassName() -> String {
		return "Pet"
	}

}
