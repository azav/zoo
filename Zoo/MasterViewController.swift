//
//  MasterViewController.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/27/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit
import Parse

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFPet]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.registerTableViewCells()
		// Do any additional setup after loading the view.

		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}
	



	func registerTableViewCells() {
		let bidCell = UINib(nibName: "BidTVCell", bundle: nil)
		self.tableView.register(bidCell, forCellReuseIdentifier: "BidTVCell")
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		fetchObject()
	}
	
	// получение объектов в таблицу
	
	private func fetchObject() {
		let query = PFPet.query()!
		query.order(byDescending: "createdAt")
		query.findObjectsInBackground { (object, error) in
			if let realObject = object as? [PFPet] {
				self.objects = realObject
				self.tableView.reloadData()

			}
		}
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
			let object = objects[indexPath.row]
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "showDetail", sender: nil)
	}
	
	@IBAction func openPageCreateBid(_ sender: UIBarButtonItem) {
		performSegue(withIdentifier: "createBidPage", sender: nil)
	}
	
	// MARK: - Table View

//	override func numberOfSections(in tableView: UITableView) -> Int {
//		return 1
//	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BidTVCell", for: indexPath) as! BidTVCell

		let object = objects[indexPath.row]
		cell.name.text = object.name
		cell.bidDate.text = object.createdAt?.description
		cell.price.text = object.price! + " грн"
		cell.autor.text = object.userName
	//	let urlImage = ""
	//	cell.bidImage?.sd_setImage(with: urlImage, completed: nil)
		
		// cell.bidDate.text = object.description
		// cell.textLabel!.text = object.name
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}


}

