//
//  AuthTVC.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/29/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit
import Parse

class AuthTVC: UITableViewController {
	
	@IBOutlet weak var email: UITextField!
	@IBOutlet weak var password: UITextField!
	@IBOutlet weak var name: UITextField!
	override func viewDidLoad() {
		super.viewDidLoad()
		
	}
	
	@IBAction func registerPressed() {
		searchUser()
	}
	func registerOrLogin() {
		let user = PFUser()
		user.username = name.text!
		user.password = password.text!
		user.email =  email.text!
		user.signUpInBackground { (succeeded, error) in
			if let error = error {
				let errorString = error.localizedDescription
				print(errorString)
			} else {
				self.dismiss(animated: true, completion: {
				})
			}
		}
	}
	
	func saveAlert(_ title: String, _ message: String) {
		let messageTitle = title
		let messageText = message
		let alert = UIAlertController(title: messageTitle, message: messageText, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
	
	func searchUser() {
		let queryName = PFUser.query()
		let queryEmail = PFUser.query()
		let queryPass = PFUser.query()
		queryName?.whereKey("username", equalTo: name.text!)
		queryEmail?.whereKey("email", equalTo: email.text!)
		queryPass?.whereKey("password", equalTo: password.text!)
		let queryResultName = try? queryName?.getFirstObject()
		let queryResultEmail = try? queryEmail?.getFirstObject()
		if (queryResultName != nil && queryResultEmail != nil) {
			if queryPass != nil {
			saveAlert("Ошибка", "Такой пользователь уже существует! Введите другое имя пользователя и e-mail")
			} else{
				self.dismiss(animated: true, completion: {
				})
			}
		} else {
			registerOrLogin()
		}
	}
}
