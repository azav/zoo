//
//  CreateTVC.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/27/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit
import Parse

class CreateTVC: UITableViewController {
	var objects = [UIImage]()
	
	@IBOutlet weak var name: UITextField!
	@IBOutlet weak var descript: UITextView!
	@IBOutlet weak var price: UITextField!
	@IBOutlet weak var phone: UITextField!
	@IBOutlet weak var photoFromLibrary: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()

	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if PFUser.current() == nil {
			performSegue(withIdentifier: "authPage", sender: nil)
		}
		
	}
	
	@IBAction func saveBid(_ sender: UIBarButtonItem) {
		if self.insertNewObject(UIBarButtonItem.self) {
			saveAlert("Ошибка!", "Объявление не сохранено!")
		}
		returnBack()
	}
	func returnBack() {
		_ = navigationController?.popViewController(animated: true)
	}

	func saveAlert(_ title: String, _ message: String) {
		let messageTitle = title
		let messageText = message
		let alert = UIAlertController(title: messageTitle, message: messageText, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
		present(alert, animated: true, completion: nil)
	}
	
	@objc func insertNewObject(_ sender: Any) -> Bool {
		let newPet = PFPet()
		var sendSucccess = false
		newPet.name = name.text
		newPet.descript = descript.text
		newPet.price = price.text
		newPet.phone = phone.text
	//	newPet.owner = PFUser.current()
	//	newPet.pictureFile = success ? file : nil
		newPet.deviceId = UIDevice.current.identifierForVendor!.description
		newPet.userId = PFUser.current()?.objectId
		newPet.userName = PFUser.current()?.username
		newPet.saveEventually { (success, error) in
			if success {
				sendSucccess = true
			}
		}
		return sendSucccess
	}
	
	@IBAction func addPhoto(_ sender: Any) {
		let picker = UIImagePickerController()
		picker.delegate = self
		picker.sourceType = .photoLibrary
		self.present(picker, animated: true, completion: nil)
	}
	// MARK: - Table view data source
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 1
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		return 6
	}
	
	/*
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
	let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
	
	// Configure the cell...
	
	return cell
	}
	*/
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
	if editingStyle == .delete {
	// Delete the row from the data source
	tableView.deleteRows(at: [indexPath], with: .fade)
	} else if editingStyle == .insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
	// Return false if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension CreateTVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	@objc  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		picker.dismiss(animated: true, completion: nil)
			  guard let image = info[.originalImage] as? UIImage else {
				  fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
			  }
			  self.photoFromLibrary!.image = image
		  }
//	@objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//		picker.dismiss(animated: true, completion: nil)
//		let image = info[.originalImage] as! UIImage
//
//		objects.insert(image, at: 0)
//		imageViewForPhoto.image =
//
//	//	let indexPath = IndexPath(row: 5, section: 0)
//	//	tableView.insertRows(at: [indexPath], with: .automatic)
//		}
	
	}
