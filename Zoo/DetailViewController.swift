//
//  DetailViewController.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/27/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
	
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var photoBid: UIImageView!
	@IBOutlet weak var priceBid: UILabel!
	@IBOutlet weak var descript: UITextView!
	@IBOutlet weak var phone: UILabel!
	
//	@IBOutlet weak var detailDescriptionLabel: UILabel!
	
	var detailItem: PFPet? {
		didSet {
			// Update the view.
			configureView()
		}
	}

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = name {
		        label.text = detail.name
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
	}
	
}

