//
//  BidTVCell.swift
//  Zoo
//
//  Created by Anton Zavgorodniy on 5/27/19.
//  Copyright © 2019 Anton Zavgorodniy. All rights reserved.
//

import UIKit

class BidTVCell: UITableViewCell {
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var price: UILabel!
	@IBOutlet weak var bidImage: UIImageView!
	@IBOutlet weak var bidDate: UILabel!
	@IBOutlet weak var autor: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
